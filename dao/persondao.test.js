var mysql = require("mysql");

const PersonDao = require("./persondao.js");
const runsqlfile = require("./runsqlfile.js");

// GitLab CI Pool
/*
var pool = mysql.createPool({
  connectionLimit: 1,
  host: "mysql.stud.iie.ntnu.no", //"localhost" if u wonna be on local db
  user: "sindrhpa",
  password: "6XgiA9XF",
  database: "sindrhpa",
  debug: false,
  multipleStatements: true
});*/
/*
var pool = mysql.createPool({
  connectionLimit: 1,
  host: "localhost", //"localhost" if u wonna be on local db
  user: "root",
  password: "sindre123",
  database: "sysut2_ov5",
  debug: false,
  multipleStatements: true
});*/

var pool = mysql.createPool({
  connectionLimit: 1,
  host: "mysql", //"localhost" if u wonna be on local db
  user: "root",
  password: "secret",
  database: "supertestdb",
  debug: false,
  multipleStatements: true
});

let personDao = new PersonDao(pool);

beforeAll(done => {
  runsqlfile("dao/create_tables.sql", pool, () => {
    runsqlfile("dao/create_testdata.sql", pool, done);
  });
});

test("get one person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].navn).toBe("Hei Sveisen");
    done();
  }

  personDao.getOne(1, callback);
});

test("get unknown person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(0);
    done();
  }

  personDao.getOne(0, callback);
});

test("add person to db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.affectedRows).toBeGreaterThanOrEqual(1);
    done();
  }

  personDao.createOne(
    { navn: "Nils Nilsen", alder: 34, adresse: "Gata 3" },
    callback
  );
});

test("get all persons from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data.length=" + data.length
    );
    expect(data.length).toBeGreaterThanOrEqual(2);
    done();
  }

    personDao.getAll(callback);
});

//Øving5: Skrevet selv
  test("update person in db", done => {
    personDao.updateOne(
      1,
      { navn: "Bob Bobsen", alder: 50, adresse: "Gata 2" },
      callback =>{
        personDao.getOne(1, (status, data) =>{
          console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
          );
          expect(data[0].navn).toBe("Bob Bobsen");
          done();
        })
      }
    );
  });

test("delete user from db", done => {
    var val = 0;
    personDao.getAll((status, data) => {
        val = data.length;
        console.log(
          "Test callback: status=" + status + ", data.length=" + data.length
        );
        personDao.deleteOne(1, callback => {
          console.log(
            "Test callback: status=" + callback.status
          );
            personDao.getAll((status, data)=> {
              console.log(
                "Test callback: status=" + status + ", length=" + data.length
              );
                expect(data.length).toBe(val - 1);
                done();
            });
        });
    });
});
