var express = require("express");
var mysql = require("mysql");
var bodyParser = require("body-parser");
var app = express();
var apiRoutes = express.Router();
app.use(bodyParser.json()); // for å tolke JSON
const PersonDao = require("./dao/persondao.js");

/*
var pool = mysql.createPool({
  connectionLimit: 2,
  host: "mysql.stud.iie.ntnu.no",
  user: "nilstesd",
  password: "lqqWcMzq",
  database: "nilstesd",
  debug: false
});*/

var pool = mysql.createPool({
  connectionLimit: 2,
  host: "databases-auth.000webhost.com",
  user: "id7459014_admin",
  password: "admin",
  database: "id7459014_classicwow",
  debug: false
});

let personDao = new PersonDao(pool);

app.get("/person", (req, res) => {
  console.log("/person: fikk request fra klient");
  personDao.getAll((status, data) => {
    res.status(status);
    res.json(data);
  });
});

app.get("/person/:personId", (req, res) => {
  console.log("/person/:personId: fikk request fra klient");
  personDao.getOne(req.params.personId, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Oppgave
app.post("/person", (req, res) => {
  console.log("Fikk POST-request fra klienten");
  personDao.createOne(req.body, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

//herfa
app.put("/person/:personId", (req, res) => {
  console.log("/person/:personId: update-request fra klienten");
  personDao.updateOne(req.params.personId, req.body, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

app.delete("/person/:personId", (req, res) => {
  console.log("/person/:personId: delete-request fra klienten");
  personDao.deleteOne(req.params.personId, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

//testing
app.get("/test", (req, res) => {
  console.log("/test: fikk request fra klient");
  personDao.getTest((status, data) => {
    res.status(status);
    res.json(data);
  });
});

var server = app.listen(8080);
